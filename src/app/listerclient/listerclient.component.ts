import { Component, OnInit } from '@angular/core';
import { ClientserviceService } from '../clientservice.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { Client } from '../client';
import { PrbComponent } from '../prb/prb.component';

@Component({
  selector: 'app-listerclient',
  templateUrl: './listerclient.component.html',
  styleUrls: ['./listerclient.component.css']
})
export class ListerclientComponent implements OnInit {

  constructor(private clientservice :ClientserviceService, public dialog: MatDialog) { }
  datasrc:MatTableDataSource <any>;
  displayedColumns: string[] = ['idCli','nomCli','prenomCli','cin','supprimer','modifier'];


  
  ngOnInit() {
    this.clientservice.getAllClient().subscribe(res=>
  { this.datasrc=new MatTableDataSource(res as Client[]);
    
  console.log(this.datasrc);
  }
    );

  }
  suppelement(idCli)
  {
     var idc=parseInt(idCli);
     console.log('idCli=',idCli)
     this.clientservice.deleteclient(idc).subscribe(res=>
      {
        this.ngOnInit();
      });
      window.alert('Le client a ete supprime')
     
  }
  openDialog(elt): void {
    
    const dialogRef = this.dialog.open(PrbComponent, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }
  applyFilter(filterValue: string) {
    this.datasrc.filter = filterValue.trim().toLowerCase();
  }

}
