import { Component, OnInit } from '@angular/core';
import { Produit } from '../produit';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ServiceProduitService } from '../service-produit.service';

@Component({
  selector: 'app-ajouterproduit',
  templateUrl: './ajouterproduit.component.html',
  styleUrls: ['./ajouterproduit.component.css']
})
export class AjouterproduitComponent implements OnInit {
  formaproduit:Produit;
  formee:FormGroup;
  constructor(private produitservice : ServiceProduitService ,private Builder:FormBuilder) { }

  ngOnInit() {
    this.formee=this.Builder.group(
      {
        idp:new FormControl(0),
        nomp:new FormControl(),
        ref:new FormControl(),
        prix:new FormControl(0),
      }
    )
  
    }
    submit(){
    
      this.formaproduit=this.formee.value;
        this.produitservice.postproduit(this.formaproduit).subscribe(res=>{
          this.produitservice.getAllProduit();
          window.alert('bien ajoute')
          this.formee=this.Builder.group(
            {
              idp:new FormControl(0),
              nomp:new FormControl(),
              ref:new FormControl(),
              prix:new FormControl(0),
            }
          
          
          )},
        
        
        err=>{
          console.log(err)
          
        })
        
      } 

}
