import { Component, OnInit } from '@angular/core';
import { FormControl, FormBuilder, FormGroup } from '@angular/forms';
import { MatAutocomplete } from '@angular/material';
import { Client } from '../client';
import { ClientserviceService } from '../clientservice.service';
import { ServiceProduitService } from '../service-produit.service';
import { Produit } from '../produit';
import { User } from '../user';
import { UserserviceService } from '../userservice.service';
import { Commande } from '../commande';
import { CommandeserviceService } from '../commandeservice.service';

@Component({
  selector: 'app-ajoutercommande',
  templateUrl: './ajoutercommande.component.html',
  styleUrls: ['./ajoutercommande.component.css']
})
export class AjoutercommandeComponent implements OnInit {
  // myControl = new FormControl();
  // myControle = new FormControl();

  options: Client[];
  optionss: Produit[];
  optionsss:User[]
  formee:FormGroup;
 
  formacommande:Commande;
 
  constructor(private Commandeservice:CommandeserviceService,private clientservice : ClientserviceService,private produitservice:ServiceProduitService,private Userservice:UserserviceService ,private Builder:FormBuilder) { }

  ngOnInit() {
    this.clientservice.getAllClient().subscribe(
      res=>{
        this.options=res as Client[];
      },
      err=>{
        console.log(err);
      }
    )


    this.produitservice.getAllProduit().subscribe(
      res=>{
        this.optionss=res as Produit[];
      },
      err=>{
        console.log(err);
      }
    )

    this.Userservice.getAllUsers().subscribe(
      res=>{
        this.optionsss=res as User[];
      },
      err=>{
        console.log(err);
      }
    )
    

   
    this.formee=this.Builder.group(
      {
        idC:new FormControl(0),
        idCli:new FormControl(),
        idp:new FormControl(),
        idUeser:new FormControl(),
        qte:new FormControl(0),
      }
    )

    
  }
  submit(){
    
    this.formacommande=this.formee.value;
      this.Commandeservice.postCommande(this.formacommande).subscribe(res=>{
        this.Commandeservice.getAllCommande();
        window.alert('bien ajoute')
        this.formee=this.Builder.group(
          { 
            idC:new FormControl(0),
            idCli:new FormControl(),
            idp:new FormControl(),
            idUeser:new FormControl(),
            qte:new FormControl(0),
          
          }
        
        
        )},
      
      
      err=>{
        console.log(err)
        
      })
      
    } 

}
