import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { ServiceProduitService } from '../service-produit.service';
import { Produit } from '../produit';
import { Prb1Component } from '../prb1/prb1.component';


@Component({
  selector: 'app-listerproduit',
  templateUrl: './listerproduit.component.html',
  styleUrls: ['./listerproduit.component.css']
})
export class ListerproduitComponent implements OnInit {
  displayedColumns: string[] = ['idp', 'nomp', 'ref', 'prix','supprimer','modifier','imprimer'];
  datasrc:MatTableDataSource <any>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(private produitservice :ServiceProduitService, public dialog: MatDialog) { }

  ngOnInit() {
    this.produitservice.getAllProduit().subscribe(res=>
  { this.datasrc=new MatTableDataSource(res as Produit[]);
    this.datasrc.paginator = this.paginator;
  console.log(this.datasrc);
  }
    );

  }
  applyFilter(filterValue: string) {
    this.datasrc.filter = filterValue.trim().toLowerCase();
  }
  suppelement(idp)
  {
     var id=parseInt(idp);
     console.log('idp=',idp)
     this.produitservice.deleteproduit(id).subscribe(res=>
      {
        this.ngOnInit();
      });
      window.alert('Le produit a ete supprime')
     
  }

  openDialog(elt): void {
    const dialogRef = this.dialog.open(Prb1Component, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }

    
}
