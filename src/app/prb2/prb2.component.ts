import { Component, OnInit, Inject } from '@angular/core';
import { UserserviceService } from '../userservice.service';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-prb2',
  templateUrl: './prb2.component.html',
  styleUrls: ['./prb2.component.css']
})
export class Prb2Component implements OnInit {

  formee: any;
  formauser: any;
  constructor(private userservice :UserserviceService ,private Builder:FormBuilder ,
    @Inject(MAT_DIALOG_DATA) public data:any, public dialogRef: MatDialogRef<Prb2Component> ) { }


    ngOnInit() {
      console.log(this.data);
      this.formee=this.Builder.group(
        {
          idUser:[this.data.idUser],
          nomUser:[this.data.nomUser],
          prenomUser:[this.data.prenomUser],
          usser:[this.data.usser],
          passeword:[this.data.passeword],
         
        }
       
      )
      console.log(this.data.idUser);
    }
    get f(){
      return this.formee.controls
    }
    onNoClick(): void {
     
      this.dialogRef.close()
    }
    submit()
    {
      
       this.formauser=this.formee.value;
       console.log(this.formauser);
       this.userservice.puttUsers(this.data.idUser,this.formauser).subscribe(res=>
        {
          this.onNoClick();
        });
       
    }

}
