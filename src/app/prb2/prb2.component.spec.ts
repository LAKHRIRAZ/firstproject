import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Prb2Component } from './prb2.component';

describe('Prb2Component', () => {
  let component: Prb2Component;
  let fixture: ComponentFixture<Prb2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Prb2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Prb2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
