import { Injectable } from '@angular/core';
import { Client } from './client';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClientserviceService {
  url:string="http://localhost:57255/api/Clients";
  clients:Client[];
  cliient:Client;
  constructor(private http:HttpClient) { }
  getAllClient()
  {
    return   this.http.get(this.url)
    
  }
  ///ajouter
  postclient(cmd:Client)
  {
    return this.http.post(this.url,cmd);
  }
//// modifier
  puttclient(id,client:Client)
  {
    return this.http.put(`${this.url}/${id}`, client);
  }
  ///delete
  deleteclient(id)
  {
    return this.http.delete(this.url+"/"+id);
  }
}
