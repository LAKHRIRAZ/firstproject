import { Component, OnInit, Inject } from '@angular/core';
import { ClientserviceService } from '../clientservice.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-prb',
  templateUrl: './prb.component.html',
  styleUrls: ['./prb.component.css']
})
export class PrbComponent implements OnInit {
  formee: any;
  formaclient: any;
  constructor(private clientservice :ClientserviceService ,private Builder:FormBuilder ,
    @Inject(MAT_DIALOG_DATA) public data:any, public dialogRef: MatDialogRef<PrbComponent> ) { }



    ngOnInit() {
      console.log(this.data);
      this.formee=this.Builder.group(
        {
          idCli:[this.data.idCli],
          nomCli:[this.data.nomCli],
          prenomCli:[this.data.prenomCli],
          cin:[this.data.cin],
         
        }
       
      )
      console.log(this.data.idCli);
    }
    get f(){
      return this.formee.controls
    }
      onNoClick(): void {
     
        this.dialogRef.close()
      }
      submit()
      {
        
         this.formaclient=this.formee.value;
         console.log(this.formaclient);
         this.clientservice.puttclient(this.data.idCli,this.formaclient).subscribe(res=>
          {
            this.onNoClick();
          });
         
      }

}
