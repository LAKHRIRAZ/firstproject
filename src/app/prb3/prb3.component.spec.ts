import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Prb3Component } from './prb3.component';

describe('Prb3Component', () => {
  let component: Prb3Component;
  let fixture: ComponentFixture<Prb3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Prb3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Prb3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
