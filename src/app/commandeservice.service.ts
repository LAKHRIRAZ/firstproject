import { Injectable } from '@angular/core';
import { Commande } from './commande';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CommandeserviceService {
  url:string="http://localhost:57255/api/Commandes";
  Commandes:Commande[]
  Commaaandes:Commande[]
  constructor(private http:HttpClient) { }

  getAllCommande()
  {
    return   this.http.get(this.url)
    
  }

   ///ajouter
   postCommande(cmd:Commande)
   {
     return this.http.post(this.url,cmd);
   }
 //// modifier
   puttCommande(id,commande:Commande)
   {
     return this.http.put(`${this.url}/${id}`, commande);
   }
   ///delete
   deleteCommande(id)
   {
     return this.http.delete(this.url+"/"+id);
   }

}
