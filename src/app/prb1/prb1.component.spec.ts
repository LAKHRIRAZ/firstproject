import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Prb1Component } from './prb1.component';

describe('Prb1Component', () => {
  let component: Prb1Component;
  let fixture: ComponentFixture<Prb1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Prb1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Prb1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
