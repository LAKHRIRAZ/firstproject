import { Component, OnInit, Inject } from '@angular/core';
import { ServiceProduitService } from '../service-produit.service';
import { FormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-prb1',
  templateUrl: './prb1.component.html',
  styleUrls: ['./prb1.component.css']
})
export class Prb1Component implements OnInit {
  formee: any;
  formaproduit: any;
  constructor(private produitservice :ServiceProduitService ,private Builder:FormBuilder ,
    @Inject(MAT_DIALOG_DATA) public data:any, public dialogRef: MatDialogRef<Prb1Component> ) { }

  

    ngOnInit() {
      console.log(this.data);
      this.formee=this.Builder.group(
        {
          idp:[this.data.idp],
          nomp:[this.data.nomp],
          ref:[this.data.ref],
          prix:[this.data.prix],
         
        }
       
      )
      console.log(this.data.idp);
    }
    get f(){
      return this.formee.controls
    }

    onNoClick(): void {
     
      this.dialogRef.close()
    }
    submit()
    {
      
       this.formaproduit=this.formee.value;
       console.log(this.formaproduit);
       this.produitservice.puttproduit(this.data.idp,this.formaproduit).subscribe(res=>
        {
          this.onNoClick();
        });
       
    }

}
