import { Injectable } from '@angular/core';
import { Produit } from './produit';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceProduitService {
  url:string="http://localhost:57255/api/Produits";
  Produits:Produit[];

  Produuuit:Produit;
  constructor(private http:HttpClient) { }
  getAllProduit()
  {
    return   this.http.get(this.url)
    
  }
   ///ajouter
   postproduit(cmd:Produit)
   {
     return this.http.post(this.url,cmd);
   }
   //// modifier
  puttproduit(id,Produit:Produit)
  {
    return this.http.put(`${this.url}/${id}`, Produit);
  }
    ///delete
    deleteproduit(id)
    {
      return this.http.delete(this.url+"/"+id);
    }
}
