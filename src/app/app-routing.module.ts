import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AjouterclientComponent } from './ajouterclient/ajouterclient.component';
import { ListerclientComponent } from './listerclient/listerclient.component';
import { ListerproduitComponent } from './listerproduit/listerproduit.component';
import { AjoutercommandeComponent } from './ajoutercommande/ajoutercommande.component';
import { ListercommandeComponent } from './listercommande/listercommande.component';
import { AjouterproduitComponent } from './ajouterproduit/ajouterproduit.component';
import { AjouteruserComponent } from './ajouteruser/ajouteruser.component';
import { ListeruserComponent } from './listeruser/listeruser.component';



const routes: Routes = [
  {
    path:'ajouterclient',
    component:AjouterclientComponent,
     pathMatch:'full'
  
  },
  {
    path:'listerclient',
    component:ListerclientComponent,
     pathMatch:'full'
  
  },
  {
    path:'ajouterproduit',
    component:AjouterproduitComponent,
     pathMatch:'full'
  
  },
  {
    path:'listerproduit',
    component:ListerproduitComponent,
     pathMatch:'full'
  
  },

  {
    path:'ajoutercommande',
    component:AjoutercommandeComponent,
     pathMatch:'full'
  
  },
  {
    path:'listercommonde',
    component:ListercommandeComponent,
     pathMatch:'full'
  
  },
  {
    path:'ajouteruser',
    component:AjouteruserComponent,
     pathMatch:'full'
  
  },
  {
    path:'listeruser',
    component:ListeruserComponent,
     pathMatch:'full'
  
  },
  {
    path:'',
    redirectTo:'',
    pathMatch:'full',
  
  }









];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
