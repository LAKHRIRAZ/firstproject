import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatInputModule, MatTableModule, MatDialogModule, MatIconModule, MatPaginatorModule } from '@angular/material/';
import { AjouterclientComponent } from './ajouterclient/ajouterclient.component';
import { ListerclientComponent } from './listerclient/listerclient.component';
import { AjouterproduitComponent } from './ajouterproduit/ajouterproduit.component';
import { ListerproduitComponent } from './listerproduit/listerproduit.component';
import { AjoutercommandeComponent } from './ajoutercommande/ajoutercommande.component';
import { ListercommandeComponent } from './listercommande/listercommande.component';
import { AjouteruserComponent } from './ajouteruser/ajouteruser.component';
import { ListeruserComponent } from './listeruser/listeruser.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import { HttpClientModule } from '@angular/common/http';
import { PrbComponent } from './prb/prb.component';
import { Prb1Component } from './prb1/prb1.component';
import { Prb2Component } from './prb2/prb2.component';
import { Prb3Component } from './prb3/prb3.component';

import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { PrintComponent } from './print/print.component';

@NgModule({
  declarations: [
    AppComponent,
    AjouterclientComponent,
    ListerclientComponent,

    AjouterproduitComponent,
    ListerproduitComponent,

    AjoutercommandeComponent,
    ListercommandeComponent,

    AjouteruserComponent,
    ListeruserComponent,
    PrbComponent,
    Prb1Component,
    Prb2Component,
    Prb3Component,
    PrintComponent
  ],
  imports: [
    // BrowserModule,
    // AppRoutingModule,
    // MatMenuModule,
    // MatButtonModule,
    // BrowserAnimationsModule,
    // FormsModule,
    // MatToolbarModule,
    // MatFormFieldModule
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    MatInputModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    FormsModule,ReactiveFormsModule,
    MatDialogModule,
    MatIconModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatAutocompleteModule
    
  
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents : [PrbComponent,Prb1Component,Prb2Component,Prb3Component]
})
export class AppModule { }
