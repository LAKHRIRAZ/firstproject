import { Component, OnInit } from '@angular/core';

import { MatDialog, MatTableDataSource } from '@angular/material';
import { User } from '../user';
import { UserserviceService } from '../userservice.service';
import { Prb2Component } from '../prb2/prb2.component';

@Component({
  selector: 'app-listeruser',
  templateUrl: './listeruser.component.html',
  styleUrls: ['./listeruser.component.css']
})
export class ListeruserComponent implements OnInit {

  constructor(private userservice :UserserviceService, public dialog: MatDialog) { }
  datasrc:MatTableDataSource <any>;
  displayedColumns: string[] = ['idUser','nomUser','prenomUser','usser','passeword','supprimer','modifier'];


  ngOnInit() {
    this.userservice.getAllUsers().subscribe(res=>
  { this.datasrc=new MatTableDataSource(res as User[]);
    
  console.log(this.datasrc);
  }
    );

  }
  applyFilter(filterValue: string) {
    this.datasrc.filter = filterValue.trim().toLowerCase();
  }
  suppelement(idUser)
  {
     var id=parseInt(idUser);
     console.log('idUser=',idUser)
     this.userservice.deleteUsers(id).subscribe(res=>
      {
        this.ngOnInit();
      });
      window.alert('Le produit a ete supprime')
     
  }
  openDialog(elt): void {
    const dialogRef = this.dialog.open(Prb2Component, {
      width: '600px',
      data: elt
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      
      this.ngOnInit()
    });
 
  }

}
