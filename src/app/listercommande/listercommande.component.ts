import { Component, OnInit } from '@angular/core';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { CommandeserviceService } from '../commandeservice.service';
import { Commande } from '../commande';

@Component({
  selector: 'app-listercommande',
  templateUrl: './listercommande.component.html',
  styleUrls: ['./listercommande.component.css']
})
export class ListercommandeComponent implements OnInit {

  constructor(private commandeservice :CommandeserviceService, public dialog: MatDialog) { }
  datasrc:MatTableDataSource <any>;
  displayedColumns: string[] = ['idC','idCli','idp','idUeser','qte','supprimer','modifier'];


  ngOnInit() {
    this.commandeservice.getAllCommande().subscribe(res=>
  { this.datasrc=new MatTableDataSource(res as Commande[]);
    
  console.log(this.datasrc);
  }
    );

  }
  applyFilter(filterValue: string) {
    this.datasrc.filter = filterValue.trim().toLowerCase();
  }
  suppelement(idC)
  {
     var id=parseInt(idC);
     console.log('idC=',idC)
     this.commandeservice.deleteCommande(id).subscribe(res=>
      {
        this.ngOnInit();
      });
      window.alert('La Commande a ete supprime')
     
  }
  
  

}
