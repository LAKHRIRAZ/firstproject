import { Injectable } from '@angular/core';
import { User } from './user';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserserviceService {
  url:string="http://localhost:57255/api/Users";

  Users:User[]
  Ussser:User[]

  constructor(private http:HttpClient) { }

  getAllUsers()
  {
    return   this.http.get(this.url)
    
  }
  ///ajouter
  postUsers(cmd:User)
  {
    return this.http.post(this.url,cmd);
  }
//// modifier
  puttUsers(id,user:User)
  {
    return this.http.put(`${this.url}/${id}`, user);
  }
  ///delete
  deleteUsers(id)
  {
    return this.http.delete(this.url+"/"+id);
  }

}
