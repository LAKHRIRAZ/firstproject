import { Component, OnInit } from '@angular/core';
import { Client } from '../client';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ClientserviceService } from '../clientservice.service';

interface Alert {
  type: string;
  message: string;
}
const ALERTS: Alert[] = [{
  type: 'success',
  message: 'This is an success alert',
}]

@Component({
  selector: 'app-ajouterclient',
  templateUrl: './ajouterclient.component.html',
  styleUrls: ['./ajouterclient.component.css']
})
export class AjouterclientComponent implements OnInit {

  formaclient:Client;
  formee:FormGroup;
  
  constructor(private clientservice : ClientserviceService ,private Builder:FormBuilder) { }

  
  ngOnInit() {
    this.formee=this.Builder.group(
      {
        idCli:new FormControl(0),
        nomCli:new FormControl(),
        prenomCli:new FormControl(),
        cin:new FormControl(),
      }
    )
  
    }
    submit(){
    
      this.formaclient=this.formee.value;
        this.clientservice.postclient(this.formaclient).subscribe(res=>{
          this.clientservice.getAllClient();
          window.alert('bien ajoute')
          this.formee=this.Builder.group(
            {
              idCli:new FormControl(0),
              nomCli:new FormControl(),
              prenomCli:new FormControl(),
              cin:new FormControl()
            }
          
          
          )},
        
        
        err=>{
          console.log(err)
          
        })
        
      } 

      reset()
    {
      this.formee=this.Builder.group(
        {
          idCli:new FormControl(0),
          nomCli:new FormControl(),
          prenomCli:new FormControl(),
          cin:new FormControl()
        }
      )
  
    }

}
