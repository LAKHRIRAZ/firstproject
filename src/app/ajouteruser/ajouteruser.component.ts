import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { User } from '../user';
import { ServiceProduitService } from '../service-produit.service';
import { UserserviceService } from '../userservice.service';

@Component({
  selector: 'app-ajouteruser',
  templateUrl: './ajouteruser.component.html',
  styleUrls: ['./ajouteruser.component.css']
})
export class AjouteruserComponent implements OnInit {
  formee:FormGroup;
  formauser:User;
 
  constructor(private userservice : UserserviceService ,private Builder:FormBuilder) { }

 
  ngOnInit() {
    this.formee=this.Builder.group(
      {
        idUser:new FormControl(0),
        nomUser:new FormControl(),
        prenomUser:new FormControl(),
        usser:new FormControl(),
        passeword:new FormControl(),
      }
    )
  
    }

    submit(){
    
      this.formauser=this.formee.value;
        this.userservice.postUsers(this.formauser).subscribe(res=>{
          this.userservice.getAllUsers();
          window.alert('bien ajouter')
          this.formee=this.Builder.group(
            {
              idUser:new FormControl(0),
              nomUser:new FormControl(),
              prenomUser:new FormControl(),
              usser:new FormControl(),
              passeword:new FormControl(),
            }
          
          
          )},
        
        
        err=>{
          console.log(err)
          
        })
        
      }

}
